'use strict';

require('should');

var diffText = require('../lib/diff_text');

describe('DiffArticle', function () {
    it('#exists', function () {
        diffText.should.be.ok;
        diffText.should.be.a.Function;
    });

    it('return an object', function () {
        diffText('a', 'b').should.be.an.Object;
    });

    it('#diff equal strings', function () {
        var diff = diffText('hady is', 'hady is');
        diff.similarPercent.should.eql(100);
    });

    it('#diff different strings', function () {
        var diff = diffText('one two three four', 'one two three five six seven eight');
        diff.similarPercent.should.be.above(75);
    });
});