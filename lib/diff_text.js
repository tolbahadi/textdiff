'use strict';

var _ = require('underscore'),
    diff = require('diff');

module.exports = function (text1, text2) {
    if (_.isEmpty(text1) || _.isEmpty(text2)) {
        throw 'Invalid arguments, text1: ' + text1 + ', text2: ' + text2;
    }
    var parts = diff.diffWords(text1, text2),
        similarCount = 0,
        result = {
            similarPercent: 0,
            similarPercent2: 0,
            parts: parts
        };

    _.each(parts, function (part) {
        if (!part.added && !part.removed) {
            similarCount += part.value.length;
        }
    });

    result.similarPercent = similarCount * 100 / text1.length;
    result.similarPercent2 = similarCount * 100 / text2.length;

    return result;
};
